import java.util.Objects;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {
    private final double[] parts;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        parts = new double[]{a, b, c, d};
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {
        return parts[0];
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return parts[1];
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return parts[2];
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return parts[3];
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (double d : parts) {
            if (d >= 0 && builder.length() > 0) {
                builder.append('+');
            }
            builder.append(d);
        }
        String s = builder.toString();
        s = s.replace("--", "+");
        s = s.replace("-+", "-");
        s = s.replace("+-", "-");
        s = s.replace("++", "+");
        return s;
    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {
        if (s == null || s.replaceAll(" ", "").isEmpty()) {
            throw new IllegalArgumentException("Input is empty!");
        }

        double[] doubles = new double[4];
        int index = 0;
        StringBuilder builder = new StringBuilder();

        for (char c : s.toCharArray()) {
            if (c == '+') {
                try {
                    addDouble(builder.toString(), doubles, index);
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException("Invalid input:" + s +
                            " \n String contains unexpected characters");
                }

                index++;
                builder = new StringBuilder();
                builder.append(c);
            } else if (c == '-') {
                if (builder.length() == 0) {
                    builder.append(c);
                } else if (builder.length() > 0 && builder.charAt(builder.length() - 1) == 'e') {
                    builder.append(c);
                } else if (builder.length() > 0 && builder.charAt(builder.length() - 1) != 'e') {
                    try {
                        addDouble(builder.toString(), doubles, index);
                    } catch (IllegalArgumentException e) {
                        throw new IllegalArgumentException("Invalid input:" + s +
                                " \n String contains unexpected characters");
                    }
                    index++;
                    builder = new StringBuilder();
                    builder.append(c);
                }
            } else {
                builder.append(c);
            }
        }

        try {
            addDouble(builder.toString(), doubles, index);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid input: " + s +
                    "\n String contains unexpected characters");
        }

        if (index < 3) {
            throw new IllegalArgumentException("Invalid input: " + s + "\n Not enough elements.");
        }

        return new Quaternion(doubles[0], doubles[1], doubles[2], doubles[3]);
    }

    private static void addDouble(String s, double[] doubles, int index) {
        try {
            validateInput(s, index);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e);
        }
        String valueString = s.replaceFirst("[ijk]", "");
        double value;
        try {
            value = Double.parseDouble(valueString);
            doubles[index] = value;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid input: " + s +
                    "\n Value " + valueString + " can not be parsed");
        }
    }

    private static void validateInput(String s, int index) {
        if (index == 0 && s.matches(".*[i-kI-K].*")) {
            throw new IllegalArgumentException("Invalid input: " + s +
                    "\n String contains unexpected characters");
        }
        if (index == 1 && s.matches(".*[jkJK].*")) {
            throw new IllegalArgumentException("Invalid input: " + s +
                    "\n String contains unexpected characters [jkJK]");
        }
        if (index == 2 && s.matches(".*[ikIK].*")) {
            throw new IllegalArgumentException("Invalid input: " + s +
                    "\n String contains unexpected characters [ikIK]");
        }
        if (index == 3 && s.matches(".*[ijIJ].*")) {
            throw new IllegalArgumentException("Invalid input: " + s +
                    "\n String contains unexpected characters [ijIJ]");
        }
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(getRpart(), getIpart(), getJpart(), getKpart());
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        for (double d : parts) {
            if (d < -0.0000001 || d > 0.0000001) {
                return false;
            }
        }
        return true;
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        double[] newParts = new double[4];
        newParts[0] = parts[0];
        for (int i = 1; i < parts.length; i++) {
            newParts[i] = parts[i] * -1;
        }
        return new Quaternion(newParts[0], newParts[1], newParts[2], newParts[3]);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        double[] newParts = new double[4];
        for (int i = 0; i < parts.length; i++) {
            newParts[i] = parts[i] * -1;
        }
        return new Quaternion(newParts[0], newParts[1], newParts[2], newParts[3]);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(
                getRpart() + q.getRpart(),
                getIpart() + q.getIpart(),
                getJpart() + q.getJpart(),
                getKpart() + q.getKpart()
        );
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        double a1 = getRpart();
        double b1 = getIpart();
        double c1 = getJpart();
        double d1 = getKpart();

        double a2 = q.getRpart();
        double b2 = q.getIpart();
        double c2 = q.getJpart();
        double d2 = q.getKpart();

        double a3 = a1 * a2 - b1 * b2 - c1 * c2 - d1 * d2;
        double b3 = a1 * b2 + b1 * a2 + c1 * d2 - d1 * c2;
        double c3 = a1 * c2 - b1 * d2 + c1 * a2 + d1 * b2;
        double d3 = a1 * d2 + b1 * c2 - c1 * b2 + d1 * a2;

        return new Quaternion(a3, b3, c3, d3);
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(
                getRpart() * r,
                getIpart() * r,
                getJpart() * r,
                getKpart() * r
        );
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (this.isZero()) {
            throw new RuntimeException("Quaternions real part and all the imaginary parts are (close to) zero");
        }
        double a = getRpart();
        double b = getIpart();
        double c = getJpart();
        double d = getKpart();

        double x = a * a + b * b + c * c + d * d;

        double a2 = a / x;
        double b2 = (-b) / x;
        double c2 = (-c) / x;
        double d2 = (-d) / x;

        return new Quaternion(a2, b2, c2, d2);
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return new Quaternion(
                getRpart() - q.getRpart(),
                getIpart() - q.getIpart(),
                getJpart() - q.getJpart(),
                getKpart() - q.getKpart()
        );
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Quaternion<" + q + " real part and all the imaginary parts are (close to) zero");
        }
        return this.times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Quaternion<" + q + " real part and all the imaginary parts are (close to) zero");
        }
        return q.inverse().times(this);
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {
        Quaternion q = (Quaternion) qo;
        for (int i = 0; i < parts.length; i++) {
            if (Math.abs(parts[i] - q.parts[i]) > 0.0000001) {
                return false;
            }
        }
        return true;
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        double x = this.getRpart() * q.getRpart() +
                this.getIpart() * q.getIpart() +
                this.getJpart() * q.getJpart() +
                this.getKpart() * q.getKpart();

        return new Quaternion(x, 0, 0, 0);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(getRpart(), getIpart(), getJpart(), getKpart());
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        double a = getRpart();
        double b = getIpart();
        double c = getJpart();
        double d = getKpart();
        return Math.sqrt(a * a + b * b + c * c + d * d);
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */

    public static void main(String[] arg) {
//        System.out.println(valueOf("2+3i+4j+5k"));
//        System.out.println(valueOf("-2-3i-4j-5k"));
//        System.out.println(valueOf("-2.0e-3-4.0e-1i-5.0e-2j-6.0e-3k"));
//        System.out.println(valueOf("     "));
//        System.out.println(valueOf("2+3i+4j+5kk"));
//        System.out.println(valueOf("2+3i+4j+5k+"));
        System.out.println(valueOf("2+3j+4i+5k"));
    }
}
